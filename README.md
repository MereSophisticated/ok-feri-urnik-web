# Ok Feri urnik - Web

## Struktura kode

<pre><font color="#295FCC"><b>.</b></font>
├── <font color="#295FCC"><b>app</b></font>
│   ├── app.component.html
│   ├── app.component.scss
│   ├── app.component.spec.ts
│   ├── app.component.ts
│   ├── app.module.ts
│   ├── app-routing.module.ts
│   ├── <font color="#295FCC"><b>home</b></font>
│   │   ├── home.component.html
│   │   ├── home.component.scss
│   │   ├── home.component.spec.ts
│   │   └── home.component.ts
│   ├── <font color="#295FCC"><b>log-in</b></font>
│   │   ├── log-in.component.html
│   │   ├── log-in.component.scss
│   │   ├── log-in.component.spec.ts
│   │   └── log-in.component.ts
│   ├── <font color="#295FCC"><b>schedule</b></font>
│   │   ├── <font color="#295FCC"><b>izbira-fakultete</b></font>
│   │   │   ├── autocomplete-izbira-fakultete.css
│   │   │   ├── autocomplete-izbira-fakultete.html
│   │   │   └── autocomplete-izbira-fakultete.ts
│   │   ├── <font color="#295FCC"><b>izbira-letnika</b></font>
│   │   │   ├── izbira-letnika.css
│   │   │   ├── izbira-letnika.html
│   │   │   └── izbira-letnika.ts
│   │   ├── <font color="#295FCC"><b>izbira-programa</b></font>
│   │   │   ├── autocomplete-izbira-programa.css
│   │   │   ├── autocomplete-izbira-programa.html
│   │   │   └── autocomplete-izbira-programa.ts
│   │   ├── material-module.ts
│   │   ├── schedule.component.html
│   │   ├── schedule.component.scss
│   │   ├── schedule.component.spec.ts
│   │   └── schedule.component.ts
│   └── <font color="#295FCC"><b>sign-up</b></font>
│       ├── sign-up.component.html
│       ├── sign-up.component.scss
│       ├── sign-up.component.spec.ts
│       └── sign-up.component.ts
├── index.html
├── main.ts
├── polyfills.ts
└── styles.scss
</pre>

## Opis komponent

Spletna stran je napisana s pomočjo AngularJS (JavaScript Framework).<br>
Komponente se nahajajo v src/app/<br>

### 1. Home
Lokacija: src/app/home<br>
Glavna stran.

### 2. Log-in
Lokacija: src/app/log-in<br>

### 3. Schedule
Lokacija: src/app/schedule<br>
Dodatno omogoča izbiro fakultete, letnika in programa.

##### 3.1. Izbira fakultete
Lokacija: src/app/schedule/izbira-fakultete<br>

##### 3.2. Izbira letnika
Lokacija: src/app/schedule/izbira-letnika<br>

##### 3.3. Izbira letnika
Lokacija: src/app/schedule/izbira-programa<br>

### 4. Signup
Lokacija: src/app/sign-up<br>