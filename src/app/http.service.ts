import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Faculty, Faculties, Programs, Group, Branch, Timetable} from './faculty';
import { Moment } from 'moment'; 
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  private apiServer = 'http://256bit.host:3000';

  httpOptions = {
    headers: new HttpHeaders( {
      'Content-Type': 'application/json'
    })
  }

  /* return faculty list */
  getAllFaculties(): Observable<Faculties> {
    return this.http.get<Faculties>(this.apiServer + '/faculty');
  }

  getFacultyPrograms(facultyID): Observable<Programs> {
    console.log(this.apiServer + '/faculty/' + facultyID + '/program-tree');
    return this.http.get<Programs>(this.apiServer + '/faculty/' + facultyID + '/program-tree');
  }

  getScheduleForBranch(faculty:Faculty, branch:Branch): Observable<Timetable>{
    var requestString = this.apiServer+'/timetable/branches/?fcode='+faculty.code+"&branches="+branch.name+
    "&dateFrom=2020-02-24&dateTo=2020-05-06";//"&dateFrom="+moment(faculty.date_semester_start).format("YYYY-MM-DD")+"&dateTo="+moment(faculty.date_semester_end).format("YYYY-MM-DD");
    console.log(requestString.replace(/ /g, '%20'));
    return this.http.get<Timetable>(requestString);
  }

  getSchedulerForGroups(faculty:Faculty, groups:Group[]){
    var requestString = this.apiServer + '/timetable/groups/?fcode='+faculty.code+"&groups=";
    //Add groups to request string
    for (let index = 0; index < groups.length; index++) {
      const group = groups[index];
      requestString += group.name;
      //Add colon if any groups haven't been added yet
      if(index != groups.length -1){
        requestString+=',';
      }
    }

    requestString += "&dateFrom=2020-02-24&dateTo=2020-05-06";

    console.log(requestString.replace(/ /g, '%20'));

    return this.http.get<Timetable>(requestString);



  }
}
