import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CalendarView } from 'angular-calendar';

@Component({
  selector: 'mwl-demo-utils-calendar-header',
  template: `
  <br>
  <div class="text-center">
    <div id="button-container" class="btn-group">
        <div
        class="btn btn-secondary"
        mwlCalendarPreviousView
        [view]="view"
        [(viewDate)]="viewDate"
        (viewDateChange)="viewDateChange.next(viewDate)"
        >
            Previous
        </div>
        <div
        class="btn btn-outline-secondary"
        mwlCalendarToday
        [(viewDate)]="viewDate"
        (viewDateChange)="viewDateChange.next(viewDate)"
        >
            Today
        </div>
        <div
        class="btn btn-secondary"
        mwlCalendarNextView
        [view]="view"
        [(viewDate)]="viewDate"
        (viewDateChange)="viewDateChange.next(viewDate)"
        >
            Next
        </div>

    </div>
 </div>
 <br />
  `,
})
export class CalendarHeaderComponent {
  @Input() view: CalendarView | 'week' | 'day';

  @Input() viewDate: Date;

  @Input() locale: string = 'en';

  @Output() viewChange: EventEmitter<string> = new EventEmitter();

  @Output() viewDateChange: EventEmitter<Date> = new EventEmitter();
}
