import { Moment } from 'moment'; 
export class Faculty{
    public id: number;
    public code: string;
    public name: string;
    public organisation: string;
    public city: string;
    public wtt_serial: string;
    public date_semester_start: Moment;
    public date_semester_end: Moment;
    public created_at: Moment;
  }

export class Faculties {
  public data: Faculty[];
}

export class Group {
  public id: number;
  public name: string;
  public created_at: Moment;
}
export class Branch {
  public id: number;
  public id_program: number;
  public name: string;
  public year: number;
  public created_at: Moment;
  public groups: Group[];
  
}
export class Program {
  public id: number;
  public faculty_id: number;
  public years: number;
  public name: string;
  public created_at: Moment;
  public branches: Branch[];
}

export class Programs {
  public data: Program[];
}

export class Course {
  public id: number;
  public name: string;
}

export class Place {
  public id: number;
  public name: string;
}

export class Exec_type{
  public id: number;
  public name: string;
}

export class CourseData{
  public event_id:number;
  public date:Moment;
  public time_from: String;
  public time_to: String;
  public course: Course;
  public place: Place;
  public exec_type: Exec_type;
}
export class Timetable{
  public data: CourseData[];
  /*public id:number;
  public date:Moment;
  public time_from: Moment;
  public time_to: Moment;
  public course: Course;
  public place: Place;
  public exec_type: Exec_type;*/
}