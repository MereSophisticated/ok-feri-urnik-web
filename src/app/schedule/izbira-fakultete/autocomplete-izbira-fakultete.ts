import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import {Faculty, Faculties} from 'src/app/faculty';

@Component({
  selector: 'autocomplete-izbira-fakultete',
  templateUrl: 'autocomplete-izbira-fakultete.html',
  styleUrls: ['autocomplete-izbira-fakultete.css'],
})

export class AutocompleteIzbiraFakultete implements OnInit {
  @Output() selectedFaculty = new EventEmitter<Faculty>();

  constructor(private _http: HttpService){ }

  faculties: Faculties;
  myControl = new FormControl();
  filteredFaculties: Observable<Faculty[]>;


  ngOnInit() {    
        /* fired when the component loads */
      this._http.getAllFaculties().subscribe((data: Faculties) => {
          this.faculties = data; //Bind data from API to faculties object
          //console.log(data);
          //console.log(this.faculties.data[0].name);
      });


      this.filteredFaculties = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): Faculty[] {
    const filterValue = value.toLowerCase();

    return this.faculties.data.filter(option => option.name.toLowerCase().includes(filterValue));
    //return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  /* called on select
    will be used to change schedule  */
  
  chooseFaculty(event: any, faculty:Faculty){
    if (event.source.selected) {
      //console.log("From child (izbira-fakultete):");
      //console.log(facultyID);
      this.selectedFaculty.emit(faculty);
    }
  }  
}


