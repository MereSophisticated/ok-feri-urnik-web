import {Component, Input, Output, EventEmitter, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Group } from 'src/app/faculty';

/**
 * @title Filter autocomplete
 */
@Component({
  selector: 'autocomplete-izbira-skupine',
  templateUrl: 'autocomplete-izbira-skupine.html',
  styleUrls: ['autocomplete-izbira-skupine.css'],
})
export class AutocompleteIzbiraSkupine{
  @Input('groups') groups:Group[];
  @Output() selectedGroups = new EventEmitter<Group[]>();
  sGroups:Group[] = [];

  ngOnChanges(changes: SimpleChanges){
    for(const propName in changes){
      if(changes.hasOwnProperty(propName)){
        switch(propName) {
          case 'groups': { 
           
          }
        }
      }
    }
  }
  myControl = new FormControl();

  changeSelectedGroups(event:any)
  {
    if(event.isUserInput) {
      //console.log(event.source.value, event.source.selected);
      const checkedGroup:Group = event.source.value;
      //console.log(checkedGroup.id);
      if(event.source.selected){
        this.sGroups.push(checkedGroup);
      }
      else{
        const index = this.sGroups.indexOf(checkedGroup,0);
        if(index > -1){
          this.sGroups.splice(index,1);
        }
      }
      //console.log("From child (izbira-skupine):")
      //console.log(this.sGroups);
      this.selectedGroups.emit(this.sGroups);
    }
  }
}
