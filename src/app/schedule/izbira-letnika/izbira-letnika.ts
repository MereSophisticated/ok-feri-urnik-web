import {Component, OnChanges, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import { HttpService } from 'src/app/http.service';
import {Programs, Program} from 'src/app/faculty';
interface Letnik {
  value: string;
  viewValue: string;
}

 @Component({
   selector: 'izbira-letnika',
   templateUrl: 'izbira-letnika.html',
   styleUrls: ['izbira-letnika.css'],
 })
 export class IzbiraLetnika implements OnChanges{

    programs: Programs;

   @Input('years') years:Number[];
   @Output() selectedYear = new EventEmitter<number>();

 
  ngOnChanges(changes: SimpleChanges){
    for(const propName in changes){
      if(changes.hasOwnProperty(propName)){
        switch(propName) {
          case 'programID': {
            //console.log("From child(izbira-letnika):");
            //console.log(this.years);
          }
        }
      }
    }
  }

  chooseYear(event: any, year: number){
    if (event.source.selected) {
      //console.log("From child (izbira-letnika) emit year:");
      //console.log(year);
      this.selectedYear.emit(year);
    }
  } 
}
