import { Component, ViewChild, AfterViewInit, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { CalendarView, CalendarEvent, CalendarViewPeriod, CalendarWeekViewBeforeRenderEvent, CalendarDayViewBeforeRenderEvent, DAYS_OF_WEEK } from 'angular-calendar';
import { addDays, addHours, startOfDay } from 'date-fns';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpService } from '../http.service';
import {programAndYears} from './izbira-programa/autocomplete-izbira-programa';
import { Program, Branch, Group, Faculty, Timetable } from '../faculty';
import { GlobalService } from '../global.service';
import * as moment from 'moment';
import { colors } from '../demo-utils/colors';

moment.updateLocale('si', {
    week: {
      dow: DAYS_OF_WEEK.MONDAY,
      doy: 0,
    },
  });
@Component({
    selector: 'app-page',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScheduleComponent implements AfterViewInit {
    view: CalendarView = CalendarView.Week;

    viewDate: Date = this.setToMonday(new Date());
    

    daysInWeek = 7;

    period: CalendarViewPeriod;

    // exclude weekends
    excludeDays: number[] = [0, 6];

    weekStartsOn = DAYS_OF_WEEK.MONDAY;

    
    events: CalendarEvent[];

    private refresh() {
        this.events = [...this.events];
        this.cd.detectChanges();
      }

    private destroy$ = new Subject();

    constructor(
        private _http: HttpService, 
        private globalService: GlobalService,
        private breakpointObserver: BreakpointObserver,
        private cd: ChangeDetectorRef,
        private elementRef: ElementRef,

    ){ }

    beforeViewRender(
        event:
        | CalendarWeekViewBeforeRenderEvent
        | CalendarDayViewBeforeRenderEvent
    ){
        this.period = event.period;
        this.cd.detectChanges();
    }

    ngOnInit() {
        const CALENDAR_RESPONSIVE = {
          small: {
            breakpoint: '(max-width: 576px)',
            daysInWeek: 2,
          },
          medium: {
            breakpoint: '(max-width: 768px)',
            daysInWeek: 3,
          },
          large: {
            breakpoint: '(max-width: 960px)',
            daysInWeek: 5,
          },
        };

        this.breakpointObserver
            .observe(
                Object.values(CALENDAR_RESPONSIVE).map(({ breakpoint}) => breakpoint)
            )
            .pipe(takeUntil(this.destroy$))
            .subscribe((state: BreakpointState) => {
                const foundBreakpoint = Object.values(CALENDAR_RESPONSIVE).find(
                ({breakpoint}) => !!state.breakpoints[breakpoint]
                );
                if (foundBreakpoint) {
                    this.daysInWeek = foundBreakpoint.daysInWeek;
                }else{
                    this.daysInWeek = 5;
                }
                this.cd.markForCheck()
            });
            

    }

    ngOnDestroy(){
        this.destroy$.next();
    }
    faculty:Faculty;
    facultyID: number = 1;
    programID: number = 1;
    program: Program;
    year: number = 1;
    years: Number[];
    opened = true;
    branch:Branch;
    groups:Group[];
    selectedGroups:Group[];
    timetable:Timetable;
    theme = 'light';

    ngAfterViewInit(): void {
    this.globalService.itemValue.subscribe((nextValue) => {

      if (nextValue == "true") {
        this.theme = 'light';
      } else {
        this.theme = 'dark';
      }
    })
    }

    getSelectedFaculty(faculty:Faculty){
        this.faculty = faculty;
        this.facultyID = faculty.id;
    }

    getSelectedProgramIDAndYears(input: programAndYears){
        this.program = input.program;
        this.years = input.years;
    }

    getSelectedYear(year: number){
        this.year = year;
    }

    getSelectedBranch(branch: Branch){
        this.branch = branch;
        this.groups = branch.groups;

        this._http.getScheduleForBranch(this.faculty,this.branch).subscribe((data:Timetable)=>{
            this.timetable = data;
            this.generateTimetable(this.timetable);
        });

    }

    getSelectedGroups(groups: Group[]){
        this.selectedGroups = groups;
        if(this.selectedGroups.length == 0){
            this._http.getScheduleForBranch(this.faculty,this.branch).subscribe((data:Timetable)=>{
                this.timetable = data;
                this.generateTimetable(this.timetable);
            });
        }
        else{
            this._http.getSchedulerForGroups(this.faculty,this.selectedGroups).subscribe((data:Timetable)=>{
                this.timetable = data;
                this.generateTimetable(this.timetable);

            });
        }

    }


    generateTimetable(timetable: Timetable){
        const newEvents = new Array();
        timetable.data.forEach(courseData => {
            var timeArrayFrom = courseData.time_from.split(':');
            var timeArrayTo = courseData.time_to.split(':');
            var color: any;
            if(courseData.exec_type.name == "RV"){
                color = colors.red;
            }
            else if(courseData.exec_type.name == "PR"){
                color = colors.blue;
            }
            else{
                color = colors.yellow;
            }
            let e = {
                start: addHours(startOfDay(moment(courseData.date).toDate()), Number(timeArrayFrom[0])),
                color: color,
                end: addHours(startOfDay(moment(courseData.date).toDate()), Number(timeArrayTo[0])),
                title: courseData.course.name,
                meta: {
                    id: courseData.event_id,
                    exec_type: courseData.exec_type.name,
                    place: courseData.place.name
                }
            }
            newEvents.push(e);
        });
        this.events = newEvents;
    
        this.refresh();
    }


    setToMonday(date: Date ) {
        var day = date.getDay() || 7;  
        if( day !== 1 ) 
            date.setHours(-24 * (day - 1)); 
        return date;
    }
    
}
