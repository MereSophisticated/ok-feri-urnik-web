import {Component, OnInit, Input, SimpleChanges, OnChanges, Output, EventEmitter} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Program, Branch } from 'src/app/faculty';


@Component({
  selector: 'autocomplete-izbira-podprograma',
  templateUrl: 'autocomplete-izbira-podprograma.html',
  styleUrls: ['autocomplete-izbira-podprograma.css'],
})
export class AutocompleteIzbiraPodprograma implements OnChanges {
  @Input('program') program:Program;
  @Input('year') year:number;
  @Output() selectedBranch = new EventEmitter<Branch>();

  myControl = new FormControl();
  options: string[] = ['Blockchain', 'Drone'];
  filteredBranches: Observable<Branch[]>;
  branchesOfSelectedYear: Branch[];

  /* Each year has it's own branches, so you also need to filter by year!!! */

  ngOnChanges(changes: SimpleChanges){
    for(const propName in changes){
      if(changes.hasOwnProperty(propName)){
        switch(propName) {
          case 'program': { 
            if(this.program && this.year){
              //console.log("From child (izbira-podprograma):");
              //console.log(this.program.branches);
              this.branchesOfSelectedYear = this.program.branches.filter(branch => branch.year == this.year);
              //console.log("Branches of year:");
              //console.log(this.branchesOfSelectedYear);
              this.filteredBranches= this.myControl.valueChanges
              .pipe(
                startWith(''),
                map(value => this._filter(value))
             );
            }
          }
        case 'year': { 
          if(this.program && this.year){
            //console.log("From child (izbira-podprograma):");
            //console.log(this.program.branches);
            this.branchesOfSelectedYear = this.program.branches.filter(branch => branch.year == this.year);
            //console.log("Branches of year:");
            //console.log(this.branchesOfSelectedYear);
            this.filteredBranches= this.myControl.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter(value))
          );
          }
        }
       }
      }
     }
   }


  private _filter(value: string): Branch[] {
    const filterValue = value.toLowerCase();

    return this.branchesOfSelectedYear.filter(option => option.name.toLowerCase().includes(filterValue));
  }


  chooseBranch(event: any, branch: Branch){
    if (event.source.selected) {
      //console.log("From child (izbira-podprograma):");
      //console.log(branch);
      this.selectedBranch.emit(branch);
    }
  }  
}
