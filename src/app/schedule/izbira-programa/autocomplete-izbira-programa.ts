import {Component, OnInit, Input, Output,EventEmitter,OnChanges, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { HttpService } from 'src/app/http.service';
import {Program, Programs} from 'src/app/faculty';

export interface programAndYears{
  program:Program;
  years:Number[];
}

@Component({
  selector: 'autocomplete-izbira-programa',
  templateUrl: 'autocomplete-izbira-programa.html',
  styleUrls: ['autocomplete-izbira-programa.css'],
})
export class AutocompleteIzbiraPrograma implements OnChanges{
  programs: Programs;
  myControl = new FormControl();
  filteredPrograms: Observable<Program[]>;
  output: programAndYears;

  constructor(private _http: HttpService){ }


  @Input('facultyID') facultyID:number;
  @Output() selectedProgramIDAndYears = new EventEmitter<programAndYears>();


 ngOnChanges(changes: SimpleChanges){
   for(const propName in changes){
     if(changes.hasOwnProperty(propName)){
       switch(propName) {
         // Get programs from API when faculty is changed
         case 'facultyID': { 
          this._http.getFacultyPrograms(this.facultyID).subscribe((data: Programs) => {
            ///console.log(this.facultyID);
            this.programs = data; 
            //console.log("From child(izbira-programa):");
            //console.log(this.programs.data);
            this.filteredPrograms = this.myControl.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter(value))
            );
          });
         }
      }
     }
    }
  }
  

  private _filter(value: string): Program[] {
    const filterValue = value.toLowerCase();

    return this.programs.data.filter(option => option.name.toLowerCase().includes(filterValue));
  }

  chooseProgram(event: any, programID: number){
    if (event.source.selected) {
      //console.log("From child (izbira-programa):");
      //console.log(programID);
      //this.output.programID = programID;

      const index = this.programs.data.findIndex((program: Program) => {
        return program.id == programID;
      });

      //this.output.years = [...Array(this.programs.data[index].years+1).keys()].slice(1);
      //console.log("From child (izbira-programa):");
      //console.log(this.output.years);

      this.selectedProgramIDAndYears.emit({ program: this.programs.data[index], years: [...Array(this.programs.data[index].years+1).keys()].slice(1) });
    }
  } 
}


