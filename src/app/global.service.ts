import {BehaviorSubject} from 'rxjs';   
import { Injectable } from '@angular/core';


export class GlobalService {
 itemValue = new BehaviorSubject(this.theItem);

 set theItem(value) {
   this.itemValue.next(value); // this will make sure to tell every subscriber about the change.
   localStorage.setItem('theme', value);
 }

 get theItem() {
   return localStorage.getItem('theme');
 }
}
