import { HttpClient } from '@angular/common/http';
import { Component, HostBinding, OnDestroy, OnInit, ElementRef } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Subscription } from 'rxjs';
import { GlobalService } from './global.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  constructor(
    private globalService: GlobalService) {
    this.globalService.theItem = this.theme.toString();
  }
  titleShown: string = 'dOK FERI Urnik';
  theme: boolean = true;
  image: boolean = true;
  rotate: boolean = false;
  title: string;
  changeTheme() {
    localStorage.setItem('theme', this.theme.toString());
    this.globalService.theItem = this.theme.toString();
  }
  rotateStyle() {
    this.rotate = true;
  }
  changeTitle() {
    if (this.title != "")
      this.titleShown = this.title;
  }

}
