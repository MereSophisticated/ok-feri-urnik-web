import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

import {AutocompleteIzbiraFakultete} from './schedule/izbira-fakultete/autocomplete-izbira-fakultete';
import {AutocompleteIzbiraPrograma} from './schedule/izbira-programa/autocomplete-izbira-programa';
import {AutocompleteIzbiraPodprograma} from './schedule/izbira-podprograma/autocomplete-izbira-podprograma';
import {AutocompleteIzbiraSkupine} from './schedule/izbira-skupine/autocomplete-izbira-skupine';
import {IzbiraLetnika} from './schedule/izbira-letnika/izbira-letnika';

import { LogInComponent } from './log-in/log-in.component';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './sign-up/sign-up.component';
import { GlobalService } from './global.service';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/moment';
import * as moment from 'moment';
import { DemoUtilsModule } from './demo-utils/module';
import { NavBarComponent } from './nav-bar/nav-bar.component';

export function momentAdapterFactory() {
  return adapterFactory(moment);
};


@NgModule({
    declarations: [
        AppComponent,
        ScheduleComponent,
        HomeComponent,
        AutocompleteIzbiraFakultete,
        AutocompleteIzbiraPrograma,
        AutocompleteIzbiraSkupine,
        AutocompleteIzbiraPodprograma,

        IzbiraLetnika,
        LogInComponent,
        SignUpComponent,
        NavBarComponent,
    ],
  entryComponents: [AutocompleteIzbiraFakultete,
    AutocompleteIzbiraPrograma, AutocompleteIzbiraPodprograma,
    AutocompleteIzbiraSkupine, IzbiraLetnika],
  providers: [GlobalService,
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
    imports: [
      FormsModule,
      ReactiveFormsModule,
      BrowserModule,
      AppRoutingModule,
      BrowserAnimationsModule,
      MatSidenavModule,
      MatFormFieldModule,
      MatAutocompleteModule,
      MatInputModule,
      MatSelectModule,
      CommonModule,
      MatButtonModule,
      MatIconModule,
      MatGridListModule,
      HttpClientModule,
      DemoUtilsModule,
      CalendarModule.forRoot({ provide: DateAdapter, useFactory: momentAdapterFactory })
      ],
    bootstrap: [AppComponent]
})
export class AppModule { }
