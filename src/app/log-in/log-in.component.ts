import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../global.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  theme = 'light';
  constructor(private globalService: GlobalService) {
    this.globalService.itemValue.subscribe((nextValue) => {

      if (nextValue == "true") {
        this.theme = 'light';

      } else {
        this.theme = 'dark';
      }
      // this.scheduler.theme(this.theme);



    })
  }

  ngOnInit(): void {
  }

}
